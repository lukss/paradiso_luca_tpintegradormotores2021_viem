﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolasPerseguidoras : MonoBehaviour
{

    private int hp;
    private GameObject jugador;
    public int rapidez;

    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Personaje 1");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}

