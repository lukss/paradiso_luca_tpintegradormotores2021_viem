﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public Text txtRecolectados;
    public int cont;
    public Rigidbody rb;
    public float salto;
    private bool Piso = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cont = 0;
        setearTextos();
        rb = GetComponent<Rigidbody>();

    }
    private void setearTextos()
    {
        txtRecolectados.text = "Piezas obtenidas de 14:" + cont.ToString();
        if (cont >= 14)
        {

            SceneManager.LoadScene("Ganaste");

        }
    }
    void Update()
    {
       

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetButtonDown("Jump") && (Piso || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            Piso = false;
            saltoActual++;
        }
     

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        Piso = true;
        saltoActual = 0;
    }
}
