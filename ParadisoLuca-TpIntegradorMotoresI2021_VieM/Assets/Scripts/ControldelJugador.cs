﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControldelJugador : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    private Animator anim;
    public float x, y;
    public Rigidbody rb;
    public float jumpHeight=5;
    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    bool isGrounded;
    
   
    void Start()
    {
        anim = GetComponent<Animator>();
        GestorDeAudio.instancia.PausarSonido("glory of love");
        GestorDeAudio.instancia.PausarSonido("Everybody");
        GestorDeAudio.instancia.PausarSonido("hearts");
        GestorDeAudio.instancia.ReproducirSonido("one more");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("t"))
        {
            GestorDeAudio.instancia.PausarSonido("one more");
            GestorDeAudio.instancia.PausarSonido("hearts");
            GestorDeAudio.instancia.PausarSonido("glory of love");
            GestorDeAudio.instancia.ReproducirSonido("Everybody");
        }
         if (Input.GetKey("y"))
        {
            GestorDeAudio.instancia.PausarSonido("one more");
            GestorDeAudio.instancia.PausarSonido("Everybody");
            GestorDeAudio.instancia.PausarSonido("glory of love");
            GestorDeAudio.instancia.ReproducirSonido("hearts");
        }
         if (Input.GetKey("u"))
        {
            GestorDeAudio.instancia.PausarSonido("one more");
            GestorDeAudio.instancia.PausarSonido("Everybody");
            GestorDeAudio.instancia.PausarSonido("hearts");
            GestorDeAudio.instancia.ReproducirSonido("glory of love");
        }

        if (Input.GetKey("i"))
        {
            GestorDeAudio.instancia.PausarSonido("glory of love");
            GestorDeAudio.instancia.PausarSonido("Everybody");
            GestorDeAudio.instancia.PausarSonido("hearts");
            GestorDeAudio.instancia.ReproducirSonido("one more");
        }
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);



        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (Input.GetKey("c"))
        {
            anim.SetBool("Other", false);
            anim.Play("Dance");
        }
        if (Input.GetKey("v"))
        {
            anim.SetBool("Other", false);
            anim.Play("Dance2");
        }
        if (x > 0 || x < 0 || y > 0 || y < 0)
        {
            anim.SetBool("Other", true);
        }
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (Input.GetKey("space"))
        {
            anim.Play("Jump");
           Invoke("Jum",2f) ;
        }
    }
    
    public void Jump()
    {
        rb.AddForce(Vector3.up* jumpHeight, ForceMode.Impulse);
    }

}
