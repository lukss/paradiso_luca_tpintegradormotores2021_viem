﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    // Start is called before the first frame update
    public void Jugar()
    {
        SceneManager.LoadScene("Juego");
    }

    // Update is called once per frame
   public void Salir()
    {
        Debug.Log("Sali del juego");
        Application.Quit();
    }
}
