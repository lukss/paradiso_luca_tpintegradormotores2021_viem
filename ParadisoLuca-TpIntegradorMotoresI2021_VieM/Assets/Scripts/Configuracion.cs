﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuracion : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("Resolucion")))
        {
            PlayerPrefs.SetString("Resolucion", "HD");
        }
        Debug.Log("La resolucion es:" + PlayerPrefs.GetString("Resolucion"));

    }

    // Update is called once per frame
 private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            PlayerPrefs.SetString("Resolucion ", "HD");

        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            PlayerPrefs.SetString("Resolucion ", "FULLHD");

        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            PlayerPrefs.SetString("Resolucion ", "4K");

        }
    }
}
